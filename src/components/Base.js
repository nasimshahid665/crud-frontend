import React from 'react';
import Header from './Header';

function Base({children}) {
  return (
    <div>
      <Header/>
        {children}
    </div>
  )
}

export default Base