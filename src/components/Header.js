import React from "react";
import { NavLink } from "react-router-dom";

function Header() {
  return (
    <nav class="navbar navbar-expand-sm navbar-dark bg-dark">
      <div class="container-fluid">
        <a class="navbar-brand" href="javascript:void(0)">
          My Blog
        </a>
        <button
          class="navbar-toggler"
          type="button"
          data-bs-toggle="collapse"
          data-bs-target="#mynavbar"
        >
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="mynavbar">
          <ul class="navbar-nav me-auto">
            <li class="nav-item">
              <NavLink class="nav-link" to="/about">
                About
              </NavLink>
            </li>
            <li class="nav-item">
              <NavLink class="nav-link" to="/login">
                Login
              </NavLink>
            </li>
            <li class="nav-item">
              <NavLink class="nav-link" to="/signup">
                Signup
              </NavLink>
            </li>
            <li class="nav-item dropdown">
              <a
                class="nav-link dropdown-toggle text-white"
                to="#"
                role="button"
                data-bs-toggle="dropdown"
              >
                More
              </a>
              <ul class="dropdown-menu">
                <li>
                  <NavLink class="dropdown-item" to="/service">
                    Service
                  </NavLink>
                </li>
                <li>
                  <NavLink class="dropdown-item" to="/contact">
                    Contact
                  </NavLink>
                </li>
                <li>
                  <a class="dropdown-item" href="#">
                    Youtube
                  </a>
                </li>
              </ul>
            </li>
          </ul>
          <a href="">YOUTUBE</a>
        </div>
      </div>
    </nav>
  );
}

export default Header;
