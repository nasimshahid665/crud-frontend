import logo from './logo.svg';
import './App.css';
import Login from './components/Login';
import Signup from './components/Signup';
import Service from './components/Service';
import Contact from './components/Contact';
import {Routes,Route} from "react-router-dom";
import About from './components/About';


function App() {
  return (
    <div className="App">
    <Routes>
      <Route path="/login" element={<Login/>} />
      <Route path="/signup" element={<Signup/>} />
      <Route path="/service" element={<Service/>} />
      <Route path="/contact" element={<Contact/>} />
      <Route path="/about" element={<About/>} />
    </Routes>

    </div>
  );
}

export default App;
